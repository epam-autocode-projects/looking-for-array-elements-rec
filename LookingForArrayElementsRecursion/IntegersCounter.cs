﻿using System;

namespace LookingForArrayElementsRecursion
{
    public static class IntegersCounter
    {
        /// <summary>
        /// Searches an array of integers for elements that are in <paramref name="elementsToSearchFor"/> <see cref="Array"/>, and returns the number of occurrences of the elements.
        /// </summary>
        /// <param name="arrayToSearch">One-dimensional, zero-based <see cref="Array"/> of integers to search.</param>
        /// <param name="elementsToSearchFor">One-dimensional, zero-based <see cref="Array"/> that contains integers to search for.</param>
        /// <returns>The number of occurrences of the elements that are in <paramref name="elementsToSearchFor"/> <see cref="Array"/>.</returns>
        private static int c = 0;
        private static int i = 0;
        private static int j = 0;
        private static int k = 0;

        public static int GetIntegersCount(int[] arrayToSearch, int[] elementsToSearchFor)
        {
            if (c == 0)
            {
                i = 0;
                j = 0;
                k = 0;
                c = 1;
            }

            if (arrayToSearch is null || elementsToSearchFor is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (arrayToSearch.Length == 0 || elementsToSearchFor.Length == 0)
            {
                c = 0;
                return 0;
            }

            if (j == elementsToSearchFor.Length - 1 && i == arrayToSearch.Length - 1)
            {
                c = 0;
                return k;
            }
            else if (arrayToSearch[i] == elementsToSearchFor[j] && i < arrayToSearch.Length)
            {
                k++;
            }

            if (i == arrayToSearch.Length - 1)
            {
                i = -1;
                j++;
            }

            i++;

            return GetIntegersCount(arrayToSearch, elementsToSearchFor);
        }

        /// <summary>
        /// Searches an array of integers for elements that are in <paramref name="elementsToSearchFor"/> <see cref="Array"/>, and returns the number of occurrences of the elements withing the range of elements in the <see cref="Array"/> that starts at the specified index and contains the specified number of elements.
        /// </summary>
        /// <param name="arrayToSearch">One-dimensional, zero-based <see cref="Array"/> of integers to search.</param>
        /// <param name="elementsToSearchFor">One-dimensional, zero-based <see cref="Array"/> that contains integers to search for.</param>
        /// <param name="startIndex">The zero-based starting index of the search.</param>
        /// <param name="count">The number of elements in the section to search.</param>
        /// <returns>The number of occurrences of the elements that are in <paramref name="elementsToSearchFor"/> <see cref="Array"/>.</returns>
        public static int GetIntegersCount(int[] arrayToSearch, int[] elementsToSearchFor, int startIndex, int count)
        {
            if (c == 0)
            {
                i = startIndex;
                j = 0;
                k = 0;
                c = 1;
            }

            if (arrayToSearch is null || elementsToSearchFor is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (startIndex < 0 || count < 0 || startIndex > arrayToSearch.Length || elementsToSearchFor.Length > arrayToSearch.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(arrayToSearch));
            }

            if (count == 0 || arrayToSearch.Length == 0 || elementsToSearchFor.Length == 0)
            {
                c = 0;
                return 0;
            }

            if (count == 0 && startIndex == 0)
            {
                c = 0;
                return 0;
            }

            if (j == elementsToSearchFor.Length - 1 && i == startIndex + count - 1)
            {
                if (arrayToSearch[i] == elementsToSearchFor[j])
                {
                    k++;
                }

                c = 0;
                return k;
            }

            if (arrayToSearch[i] == elementsToSearchFor[j] && i <= startIndex + count - 1)
            {
                k++;
            }

            if (i == startIndex + count - 1)
            {
                i = startIndex;
                j++;
            }

            i++;

            return GetIntegersCount(arrayToSearch, elementsToSearchFor, startIndex, count);
        }
    }
}
