﻿using System;

namespace LookingForArrayElementsRecursion
{
    public static class FloatCounter
    {
        /// <summary>
        /// Searches an array of floats for elements that are in a specified range, and returns the number of occurrences of the elements that matches the range criteria.
        /// </summary>
        /// <param name="arrayToSearch">One-dimensional, zero-based <see cref="Array"/> of single-precision floating-point numbers.</param>
        /// <param name="rangeStart">One-dimensional, zero-based <see cref="Array"/> of the range starts.</param>
        /// <param name="rangeEnd">One-dimensional, zero-based <see cref="Array"/> of the range ends.</param>
        /// <returns>The number of occurrences of the <see cref="Array"/> elements that match the range criteria.</returns>
        private static int k = 0;
        private static int j = 0;
        private static int i = 0;
        private static int endIndex = 0;

        public static int GetFloatsCount(float[] arrayToSearch, float[] rangeStart, float[] rangeEnd)
        {
            i = 0;
            j = 0;
            k = 0;

            return GetFloats(arrayToSearch, rangeStart, rangeEnd);
        }

        public static int GetFloats(float[] arrayToSearch, float[] rangeStart, float[] rangeEnd)
        {
            if (arrayToSearch is null || rangeStart is null || rangeEnd is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (rangeStart.Length == 0 && rangeEnd.Length == 0)
            {
                return 0;
            }

            if ((rangeStart[j] > rangeEnd[j]) || (rangeStart.Length != rangeEnd.Length))
            {
                throw new ArgumentException("bad");
            }

            if (arrayToSearch.Length == 0)
            {
                return 0;
            }

            if (i == arrayToSearch.Length - 1 && j == rangeStart.Length - 1)
            {
                if (arrayToSearch[i] >= rangeStart[j] && arrayToSearch[i] <= rangeEnd[j])
                {
                    k++;
                }

                return k;
            }

            if (arrayToSearch[i] >= rangeStart[j] && arrayToSearch[i] <= rangeEnd[j])
            {
                k++;
            }

            if (i == arrayToSearch.Length - 1)
            {
                j++;
                i = -1;
            }

            i++;

            return GetFloats(arrayToSearch, rangeStart, rangeEnd);
        }

        /// <summary>
        /// Searches an array of floats for elements that are in a specified range, and returns the number of occurrences of the elements that matches the range criteria.
        /// </summary>
        /// <param name="arrayToSearch">One-dimensional, zero-based <see cref="Array"/> of single-precision floating-point numbers.</param>
        /// <param name="rangeStart">One-dimensional, zero-based <see cref="Array"/> of the range starts.</param>
        /// <param name="rangeEnd">One-dimensional, zero-based <see cref="Array"/> of the range ends.</param>
        /// <param name="startIndex">The zero-based starting index of the search.</param>
        /// <param name="count">The number of elements in the section to search.</param>
        /// <returns>The number of occurrences of the <see cref="Array"/> elements that match the range criteria.</returns>
        public static int GetFloatsCount(float[] arrayToSearch, float[] rangeStart, float[] rangeEnd, int startIndex, int count)
        {
            i = startIndex;
            j = 0;
            k = 0;
            endIndex = startIndex + count - 1;

            return GetFloats(arrayToSearch, rangeStart, rangeEnd, startIndex, count);
        }

        public static int GetFloats(float[] arrayToSearch, float[] rangeStart, float[] rangeEnd, int startIndex, int count)
        {
            if (arrayToSearch is null || rangeStart is null || rangeEnd is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (startIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(arrayToSearch));
            }

            if (rangeStart.Length == 0 && rangeEnd.Length == 0)
            {
                return 0;
            }

            if (count < 0 || startIndex > arrayToSearch.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(arrayToSearch));
            }

            if (rangeStart[j] > rangeEnd[j])
            {
                throw new ArgumentException("bad");
            }

            if (rangeStart.Length != rangeEnd.Length)
            {
                throw new ArgumentException("bad one");
            }

            if (arrayToSearch.Length == 0 || count == 0)
            {
                return 0;
            }

            if (i == endIndex && j == rangeStart.Length - 1)
            {
                if (arrayToSearch[i] >= rangeStart[j] && arrayToSearch[i] <= rangeEnd[j])
                {
                    k++;
                }

                return k;
            }

            if (arrayToSearch[i] >= rangeStart[j] && arrayToSearch[i] <= rangeEnd[j])
            {
                if (rangeStart[j] > rangeEnd[j])
                {
                    throw new ArgumentException("bad");
                }

                k++;
            }

            if (i == endIndex)
            {
                j++;
                i = startIndex - 1;
            }

            i++;

            return GetFloats(arrayToSearch, rangeStart, rangeEnd, startIndex, count);
        }
    }
}
