﻿using System;
using System.Collections.Generic;

#pragma warning disable S2368

namespace LookingForArrayElementsRecursion
{
    public static class DecimalCounter
    {
        /// <summary>
        /// Searches an array of decimals for elements that are in a specified range, and returns the number of occurrences of the elements that matches the range criteria.
        /// </summary>
        /// <param name="arrayToSearch">One-dimensional, zero-based <see cref="Array"/> of single-precision floating-point numbers.</param>
        /// <param name="ranges">One-dimensional, zero-based <see cref="Array"/> of range arrays.</param>
        /// <returns>The number of occurrences of the <see cref="Array"/> elements that match the range criteria.</returns>
        private static int i = 0;
        private static int j = 0;
        private static int res = 0;
        private static int endIndex = 0;
        private static List<decimal> list = new List<decimal>();

        public static int GetDecimalsCount(decimal[] arrayToSearch, decimal[][] ranges)
        {
            i = 0;
            j = 0;
            res = 0;
            list.Clear();

            return GetDecimals(arrayToSearch, ranges);
        }

        public static int GetDecimals(decimal[] arrayToSearch, decimal[][] ranges)
        {
            if (arrayToSearch is null || ranges is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (ranges.Length > 1 && (ranges[0] is null || ranges[1] is null))
            {
                throw new ArgumentNullException(nameof(ranges));
            }

            if (ranges.Length > 1 && ranges[0].Length != ranges[1].Length && ranges[0].Length != 0 && ranges[1].Length != 0)
            {
                throw new ArgumentException("bad");
            }

            if (arrayToSearch.Length == 0 || ranges.Length == 0)
            {
                return 0;
            }

            if (arrayToSearch.Length < 0 || ranges.Length < 0)
            {
                throw new ArgumentNullException(nameof(ranges));
            }

            if (j < ranges.Length)
            {
                if (ranges[j] is null || ranges[j].Length <= 0)
                {
                    return res;
                }

                i = 0;
                Get(arrayToSearch, ranges);
                j++;
                return GetDecimals(arrayToSearch, ranges);
            }

            return res;
        }

        /// <summary>
        /// Searches an array of decimals for elements that are in a specified range, and returns the number of occurrences of the elements that matches the range criteria.
        /// </summary>
        /// <param name="arrayToSearch">One-dimensional, zero-based <see cref="Array"/> of single-precision floating-point numbers.</param>
        /// <param name="ranges">One-dimensional, zero-based <see cref="Array"/> of range arrays.</param>
        /// <param name="startIndex">The zero-based starting index of the search.</param>
        /// <param name="count">The number of elements in the section to search.</param>
        /// <returns>The number of occurrences of the <see cref="Array"/> elements that match the range criteria.</returns>
        public static int GetDecimalsCount(decimal[] arrayToSearch, decimal[][] ranges, int startIndex, int count)
        {
            i = startIndex;
            j = 0;
            res = 0;
            endIndex = startIndex + count;
            list.Clear();

            return GetDecimals(arrayToSearch, ranges, startIndex, count);
        }

        public static int GetDecimals(decimal[] arrayToSearch, decimal[][] ranges, int startIndex, int count)
        {
            if (startIndex < 0 || count < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex));
            }

            if (arrayToSearch is null || ranges is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (startIndex > arrayToSearch.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex));
            }

            if (ranges.Length > 1 && (ranges[0] is null || ranges[1] is null))
            {
                throw new ArgumentNullException(nameof(ranges));
            }

            if (ranges.Length > 1 && ranges[0].Length != ranges[1].Length && ranges[0].Length != 0 && ranges[1].Length != 0)
            {
                throw new ArgumentException("bad");
            }

            if (count > arrayToSearch.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(count));
            }

            if (arrayToSearch.Length == 0 || ranges.Length == 0)
            {
                return 0;
            }

            if (arrayToSearch.Length < 0 || ranges.Length < 0)
            {
                throw new ArgumentNullException(nameof(ranges));
            }

            if (j < ranges.Length)
            {
                if (ranges[j] is null || ranges[j].Length <= 0)
                {
                    return res;
                }

                i = startIndex;
                Get(arrayToSearch, ranges, startIndex, count);
                j++;
                return GetDecimals(arrayToSearch, ranges, startIndex, count);
            }

            return res;
        }

        private static int Get(decimal[] arrayToSearch, decimal[][] ranges, int startIndex, int count)
        {
            if (i < endIndex)
            {
                if (arrayToSearch[i] >= ranges[j][0] && arrayToSearch[i] <= ranges[j][1])
                {
                    if (!list.Contains(arrayToSearch[i]))
                    {
                        list.Add(arrayToSearch[i]);
                        res++;
                    }
                }

                i++;

                return Get(arrayToSearch, ranges, startIndex, count);
            }

            return res;
        }

        private static int Get(decimal[] arrayToSearch, decimal[][] ranges)
        {
            if (i < arrayToSearch.Length)
            {
                if (arrayToSearch[i] >= ranges[j][0] && arrayToSearch[i] <= ranges[j][1])
                {
                    if (!list.Contains(arrayToSearch[i]))
                    {
                        list.Add(arrayToSearch[i]);
                        res++;
                    }
                }

                i++;

                return Get(arrayToSearch, ranges);
            }

            return res;
        }
    }
}
